Automatically register to the KPN Fon free wifi
===============================================

Simple scripts to automatically resgister and login to KPN Fon free wifi captive portal from Linux and MacOSX

Install the requirements
------------------------

```
sudo pacman -S networkmanager
cd ~/projects/
git clone gitlab.com/loacker/auto-register-kpn-captive-portal

```

Run the script on linux
-----------------------

```
./kpn-cp-linux.sh
```

Run the script on MacOSX
------------------------

```
./kpn-cp-mac.sh
```

<!-- vim: set ts=4 sw=4 sts=4 tw=0 ff=unix ft=markdown et ai :-->
