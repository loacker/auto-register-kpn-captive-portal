#!/usr/bin/env bash

#set -x


CCOUNTER=1


check() {
   for (( cntr=1; cntr<=3; cntr++ )); do
       ping -Qoc1 8.8.8.8 > /dev/null 2>&1
       [[ $? != 0 ]] && ( clear && echo -ne "Check connection, retry...$cntr \r";  ) || break
       [[ $cntr != 3 ]] && continue || STATUS=fault; break
   done
}


main() {
    num() { python -c "import random; print(random.randrange(0, 99))"; }
    hex() { openssl rand -hex 5; }
    
    FILEQS=/tmp/file$(hex)
    FILER=/tmp/filer$(hex)
    FILERC=/tmp/filerc$(hex)
    COOKIE=/tmp/cookie$(hex)
    EMAIL="$(hex)@$(hex).$(hex).nl"
    PASSWORD="ciaociao"
    USERAGENT='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
    
    mv "/System/Library/CoreServices/Captive Network Assistant.app" "/System/Library/CoreServices/Captive Network Assistant.app.orig" > /dev/null 2>&1
    
    rm -rf /Users/matteo/Library/Cookies/Cookies.binarycookies > /dev/null 2>&1


    set_airport() { 
        `which ifconfig` en1 ether 00:$(num):$(num):$(num):$(num):$(num) > /dev/null 2>&1
        `which networksetup` -setnetworkserviceenabled "Wi-FI" off > /dev/null 2>&1
        `which networksetup` -setnetworkserviceenabled "Wi-FI" on > /dev/null 2>&1
        `which networksetup` -setairportnetwork en1 "KPN Fon" > /dev/null 2>&1
    }

    set_airport

    while true; do
        while [[ $(networksetup -getairportnetwork en1 | awk '{print $4 $5}' ) == "KPNFon" \
            && $(ifconfig en1 | awk -F"." '/169.254./ {print $2}') == "254" ]]; do
            set_airport
        done
         
        `which curl` -s -A "${USERAGENT}" -k -L http://captive.apple.com --resolve captive.apple.com:80:`netstat -rn | awk '/^default.*en1/ {print $2}'` -i -c $COOKIE > $FILEQS
        [[ $? == 0 ]] && break
    done

    
    QS=$(cat $FILEQS | awk -F=\" '/href/ && /Free/ {print $4}' | cut -d\" -f1 |sed 's/amp;//g')
    
    `which curl` -s -A "${USERAGENT}" -L -i --max-redirs 4 -b $COOKIE ${QS} --post302 --data-urlencode "register[email]=${EMAIL}" --data-urlencode "register[password]=${PASSWORD}" --data-urlencode "register[repeat-password]=${PASSWORD}" --data-urlencode "register[terms]=on" > $FILER
    
    
    cat $FILER | grep -E 'Loc.*portal' | cut -d":" -f2- | tr -d " " > $FILERC
    REDIR=$(cat -e $FILERC | tr -d '^M$')
    
    `which curl` -s -A "${USERAGENT}" -L -b $COOKIE -H "X-Forwarded-For: 31.25.212.139:443" -H "Referer: https://kpn.purchases.fon.com/en_GB/register" $REDIR > /dev/null 2>&1
    
    rm $FILEQS $COOKIE $FILER $FILERC
    
    mv "/System/Library/CoreServices/Captive Network Assistant.app.orig" "/System/Library/CoreServices/Captive Network Assistant.app"

   
 
    ping -qc1 8.8.8.8 > /dev/null 2>&1  
    if [[ $? == 0 ]]; then
        COUNTER=800
        while [  $COUNTER -gt 0 ]; do
            ( clear && echo -en "Connection number $CCOUNTER - Reset in $COUNTER \r" )
            [[ $COUNTER == 1 ]] && ( clear && echo -ne "Counter reset - Reconnecting \r" )
            sleep 1
            let COUNTER=COUNTER-1 
            check
            [[ -n $(/usr/bin/pgrep Captive\ Network\ Assistant) ]] && killall Captive\ Network\ Assistant
            [[ -n "$STATUS" ]] || continue && ( clear && echo -ne "Reconnect... \r" ); unset STATUS; break
        done
        let CCOUNTER=CCOUNTER+1
        main
    else
        main
    fi
}

main


