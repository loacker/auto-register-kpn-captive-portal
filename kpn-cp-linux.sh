#!/usr/bin/env bash

#set -x


CCOUNTER=1


check() {
   for (( cntr=1; cntr<=3; cntr++ )); do
       ping -qc1 8.8.8.8 > /dev/null 2>&1
       [[ $? != 0 ]] && ( notify-send "Check connection, retry...$cntr";  ) || break
       [[ $cntr != 3 ]] && continue || STATUS=fault; break
   done
}


main() {
    num() { python -c "import random; print(random.randrange(0, 99))"; }
    hex() { openssl rand -hex 5; }
    
    FILEQS=/tmp/file$(hex)
    FILER=/tmp/filer$(hex)
    FILERC=/tmp/filerc$(hex)
    COOKIE=/tmp/cookie$(hex)
    EMAIL="$(hex)@$(hex).$(hex).nl"
    PASSWORD="ciaociao"
    USERAGENT='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
   
    nmcli device disconnect wlan0 > /dev/null 2>&1
    nmcli connection up KPN\ Fon > /dev/null 2>&1

    while true; do
        `which curl` -s -A "${USERAGENT}" -k -L http://captive.apple.com --resolve captive.apple.com:80:`ip r | awk '/default/ {print $3}'` -i -c $COOKIE > $FILEQS
        [[ $? == 0 ]] && break
    done

    QS=$(cat $FILEQS | awk -F=\" '/href/ && /Free/ {print $4}' | cut -d\" -f1 |sed 's/amp;//g')
    
    `which curl` -s -A "${USERAGENT}" -L -i --max-redirs 4 -b $COOKIE ${QS} --post302 --data-urlencode "register[email]=${EMAIL}" --data-urlencode "register[password]=${PASSWORD}" --data-urlencode "register[repeat-password]=${PASSWORD}" --data-urlencode "register[terms]=on" > $FILER
    
    cat $FILER | grep -E 'Loc.*portal' | cut -d":" -f2- | tr -d " " > $FILERC
    REDIR=$(cat -e $FILERC | tr -d '^M$')
    
    `which curl` -s -A "${USERAGENT}" -L -b $COOKIE -H "X-Forwarded-For: 31.25.212.139:443" -H "Referer: https://kpn.purchases.fon.com/en_GB/register" $REDIR > /dev/null 2>&1
    
    rm $FILEQS $COOKIE $FILER $FILERC
    
    ping -qc1 8.8.8.8 > /dev/null 2>&1  
    if [[ $? == 0 ]]; then
        COUNTER=800
        while [  $COUNTER -gt 0 ]; do
            notify-send "Connection number $CCOUNTER - Reset in $COUNTER"
            [[ $COUNTER == 1 ]] && ( notify-send "Counter reset - Reconnecting" )
            sleep 1
            let COUNTER=COUNTER-1 
            check
            [[ -n "$STATUS" ]] || continue && ( notify-send "Reconnect..." ); unset STATUS; break
        done
        let CCOUNTER=CCOUNTER+1
        main
    else
        main
    fi
}


main


# vim: set ts=8 sw=4 sts=4 tw=79 ff=unix ft=sh et ai :
